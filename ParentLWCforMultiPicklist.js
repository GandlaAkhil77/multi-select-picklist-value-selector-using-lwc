import {
	LightningElement,
	api,
	wire,
	track
} from "lwc";
export default class ParentLWCforMultiPicklist extends NavigationMixin(LightningElement) {



	@track divClickedEvent = {};

	onDivClick(event) {

		this.divClickedEvent = event.target.getBoundingClientRect();
	}
    
	filterEvent(event) {
		this.showSpinner = true;
		//console.log('filterRecords event.detail ', event.detail);
		if (event.detail) {
			let filteredValues = event.detail.filteredValues;
			filteredValues = JSON.parse(JSON.stringify(filteredValues));
			let filterName = event.detail.filterName;
			if (filteredValues.length > 0) {
				this.filterMap[filterName] = filteredValues;
			} else {
				delete this.filterMap[filterName];
			}
			//console.log('filteredValues', filteredValues);

			//console.log('filterName', filterName);
			this.filterRecords(filterName);
		}
		this.showSpinner = false;
	}
    
    
    filterRecords(filterName) {
		let data = [];
		
		if (Object.keys(this.filterMap).length === 0 && this.filterMap.constructor === Object) {
			data = this.originalData;
		} else {
			for (let row of this.originalData) {
				let isFilterRecord = true;
				for (let filter in this.filterMap) {
					if (this.filterMap.hasOwnProperty(filter)) {
						let filteredValuesTemp = this.filterMap[filter]
						if (!filteredValuesTemp.includes(row[filter])) {
							isFilterRecord = false;
						}
					}
				}
				if (isFilterRecord === true) {
					data.push(row);
				}
			}
		}
		let searchData = [];
		// for searching the data.....
		if (this.obj.caseCoordinationSearch && this.obj.caseCoordinationSearch.length > 2) {
			for (let row of data) {
				//str.toLowerCase().includes('Stark'.toLowerCase());
				if (
					(row.patientName && row.patientName.toLowerCase().includes(this.obj.caseCoordinationSearch.toLowerCase())) ||
					(row.patientId && row.patientId.toLowerCase().includes(this.obj.caseCoordinationSearch.toLowerCase())) ||
					(row.formattedDob && row.formattedDob.toLowerCase().includes(this.obj.caseCoordinationSearch.toLowerCase())) ||
					(row.Address && row.Address.toLowerCase().includes(this.obj.caseCoordinationSearch.toLowerCase())) ||
					(row.createdDate && row.createdDate.toLowerCase().includes(this.obj.caseCoordinationSearch.toLowerCase()))
				) {
					searchData.push(row);
				}
			}
		} else {
			searchData = data;
		}
		this.data = searchData;
		this.configureFilter(filterName);

	}
    
    @track phcNames;
    @track filterMap = {};

	configureFilter(filterName) {

		
		let filteredValues = this.filterMap[filterName];
		if (filteredValues === undefined || filteredValues.length === 0) {
			filterName = '';
		}

		let phcNames = [];
		
		if (filterName !== 'phcName') {
			this.phcNames = [];
		}

		for (let row of this.data) {

			if (filterName !== 'phcName' && row.phcName && !phcNames.includes(row.phcName)) {
				phcNames.push(row.phcName);
				this.phcNames.push(this.selectOption(row.phcName));
			}
		
		}
		if (filterName !== "phcName") {
			//console.log(' cuurent filter name in phcNames', filterName);
			this.phcNames = this.formatOptions(this.phcNames);
		}

	}

	formatOptions(picklistOptions) {
		let fomattedOptions = [];
		for (let item of picklistOptions) {
			item = JSON.parse(JSON.stringify(item));
			fomattedOptions.push(item);
		}
		fomattedOptions.sort((a, b) => ((a.value > b.value) ? 1 : -1));
		return fomattedOptions;
	}


	selectOption(value) {
		let option = {};
		option.value = value;
		option.label = value;
		option.isChecked = false;
		return option;
	}
}