import {
	LightningElement,
	track,
	api
} from 'lwc';

export default class NmMultilselectComponent extends LightningElement {

	@track selectValues = [];
	//@track picklistOptions = [];
	@track multiSelectedValues= '';
	@api label = 'Select an Option';
	@api placeholder = 'Select an Option';
	@track openMultiSelectPicklistDropdown = false;
	@api filterName;
	@api filterMap ={};
	@track isDisabled = false;
	//@api picklistOptions = [];

	

	//public class NAICSCodes{
    //    @AuraEnabled public String value;
    //    @AuraEnabled public Boolean isChecked;
    //    public NAICSCodes(){
    //        isChecked = true;
    //    }
        
	//}
	connectedCallback(){
		//console.log('picklistOptions', this.picklistOptions);
	}

	//@api   picklistOptions =  
    //     [
    //        { label: 'New', value: 'new', isChecked: false },
    //        { label: 'In Progress', value: 'inProgress', isChecked: false },
    //        { label: 'Finished', value: 'finished', isChecked: false },
    //    ];
	
	@track _picklistOptions;
	@api
	get picklistOptions() {
		return this._picklistOptions;
	}
	set picklistOptions(x) {
		//console.log('x',x);
		this._picklistOptions = JSON.parse(JSON.stringify(x));
		if(this._picklistOptions.length >0)
			this.isDisabled = false;
		else
			this.isDisabled = true;
		
		//console.log('this.filterMap', this.filterMap);
		//console.log('this.filterMap', JSON.parse(JSON.stringify(this.filterMap)));
		//console.log('this.filterName', this.filterName);
		let filteredValues = this.filterMap[this.filterName];
		//console.log('filteredValues', filteredValues);
		filteredValues = (filteredValues === undefined) ? [] : filteredValues;
		//console.log('filteredValues', JSON.parse(JSON.stringify(filteredValues)));
		let count = 0;
		for (let item of this._picklistOptions) {
			if (filteredValues.includes(item.value)) {
				item.isChecked = true;
				count ++;
			}
		}
		if(count === 0){
			this.multiSelectedValues = "Select " + this.label;
		}else{
			this.multiSelectedValues = count +  " " + this.label +"  Selected";
		}
	
	}

	@track _divClickedEvent;

	@api
	get divClickedEvent() {
		return this._divClickedEvent;
	}
	set divClickedEvent(x) {
		//console.log('x',x);
		this._divClickedEvent = x;
		if(x){
			this.processClickEvent(x);
		}
	}


	handleItemChange(event) {
		let selectedValue = event.target.name;
		let isChecked = event.target.checked;
		let filteredValues = [];
		let allValues;
		if (selectedValue === "Select All") {
			for (let item of this._picklistOptions) {
				item.isChecked = isChecked;
				if (isChecked === true) {
					filteredValues.push(item.value);
				}
			}
		} else {
			for (let item of this._picklistOptions) {
				//console.log("item.value", item.value);
				//console.log("selectedValue", selectedValue);
				
				if (item.value === selectedValue) {
					item.isChecked = isChecked;
				}
				if (item.isChecked === true && item.value !== "Select All") {
					filteredValues.push(item.value);
				}
				if (item.value === "Select All") {
					allValues = item;
				}
				//console.log("item.isChecked", item.isChecked);
			}
		}
		if (allValues) {
			allValues.isChecked = false;
		}
		this.filterPicklistValues(filteredValues, selectedValue);
	}

	filterPicklistValues(filteredValues, selectedValue) {
		//console.log("filteredValues", filteredValues, "selectedValue", selectedValue);
		
		if (selectedValue === "Select All" && filteredValues.length > 0) {
			//this.companyList = this.originalData;
			if (filteredValues.length > 0)
				this.multiSelectedValues =
				filteredValues.length - 1 + " "+ this.label +" Selected";
			else {
				this.multiSelectedValues = "Select "+ this.label ;
			}
		} else {
			//console.log("this.originalData", this.originalData);
			if (filteredValues.length > 0)
				this.multiSelectedValues =
				filteredValues.length +  " " + this.label +"  Selected";
			else {
				this.multiSelectedValues = "Select "+ this.label;
			}
		}
		const selectEvent = new CustomEvent("selectevent", {
			detail: {
				filteredValues: filteredValues,
				filterName: this.filterName
			}
		});
		this.dispatchEvent(selectEvent);
	}


	openMultiSelectPicklist(event) {
		if(this.isDisabled === false){
			this.openMultiSelectPicklistDropdown = true;
		}
	}

	closeMultiSelectPicklist(event) {
		this.openMultiSelectPicklistDropdown = false;
	}


	onDivClick(event) {
		let clickedElement = event.target.getBoundingClientRect();
		this.processClickEvent(clickedElement);
	}
	

	processClickEvent(clickedElement) {
		if (this.openMultiSelectPicklistDropdown === true) {

			//console.log(clickedElement.x, clickedElement.y);
			let parentDiv = this.template.querySelector(".dropdown-div");
			let dropdownDiv = this.template.querySelector(".c-div-dropdown");
			if (typeof parentDiv.getBoundingClientRect == 'function') {
				let parentDivCordinates = parentDiv.getBoundingClientRect();
				//console.log(parentDivCordinates.x, parentDivCordinates.y);
				//console.log(parentDiv.offsetWidth, parentDiv.offsetHeight);
				if (
					clickedElement.x < parentDivCordinates.x ||
					clickedElement.x > parentDivCordinates.x + parentDiv.offsetWidth
				) {
					//console.log('outside cilcked');
					this.openMultiSelectPicklistDropdown = false;
				} else if (
					clickedElement.y < parentDivCordinates.y ||
					clickedElement.y >
					parentDivCordinates.y +
					parentDiv.offsetHeight +
					dropdownDiv.offsetHeight
				) {
					//console.log('outside cilcked');
					this.openMultiSelectPicklistDropdown = false;
				} else {
					//console.log('inside cilcked');
				}
			}

		}
	}

}